import { axiosInstance } from "../config";

export const categoryTab = () => {
  const response = axiosInstance.get("doctors/category");
  return response;
};

export const getDokter = (keyword, cat_id) => {
  const response = axiosInstance.get(
    `doctors?search=${keyword}&category_id=${cat_id}`
  );
  return response;
};
export const getDetailDokter = id => {
  const response = axiosInstance.get(`doctors/${id}`);
  return response;
};
