import { axiosInstance } from "../config";

export const register = async data => {
  const response = axiosInstance.post("register", data);
  return response;
};
