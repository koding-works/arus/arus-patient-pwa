import React, { useState } from "react";
import { Container, Typography } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Toolbar } from "@material-ui/core";
import LogoOtp from "../../assets/otp-phone.png";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
import InputAdornment from "@material-ui/core/InputAdornment";
import useStyles from "./style";

function SplashScreen(props) {
  const classes = useStyles();
  const [otp, setOtp] = useState("");
  const [data, setData] = useState({
    phone: ""
  });

  const handleChange = e => {
    const newData = { ...data, [e.target.name]: e.target.value };
    setData(newData);
    console.log(newData, "tes");
  };

  // const handleChange = () => {
  //   setOtp({ otp });
  // };
  const handleKodeOTP = () => {
    props.history.push("/otp/kode-otp");
  };

  const handleBack = () => {
    props.history.push("/login");
  };
  return (
    <Container maxWidth="xs" className={classes.Container}>
      <Grid container>
        <AppBar
          position="static"
          className={classes.appbar}
          onClick={handleBack}
        >
          <Toolbar>
            <ArrowBackIcon />
          </Toolbar>
        </AppBar>
        <img src={LogoOtp} alt="login otp" className={classes.otp} />
      </Grid>

      <Grid container spacing={0}>
        <Grid item xs={12} className={classes.gridText}>
          <Typography className={classes.text}>Nomor Telepon</Typography>
          <Typography className={classes.kode}>
            4-digit kode OTP akan dikirimkan via SMS ke nomor telepon Anda
          </Typography>
        </Grid>

        <Grid item xs={12} className={classes.inputGrid}>
          <TextField
            placeholder="Phone Number"
            type="number"
            onChange={handleChange}
            value={data.phone}
            name="phone"
            fullWidth={true}
            style={{ width: "90%" }}
            margin="normal"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Typography style={{ color: "#25282B" }}>+62</Typography>
                </InputAdornment>
              )
            }}
          />
        </Grid>
      </Grid>

      <Grid container spacing={0} className={classes.gridButton}>
        <Button
          disableRipple={true}
          id="submit-button"
          className={classes.buttonBottom}
          style={{
            backgroundColor: "#F7A647"
          }}
          onClick={handleKodeOTP}
        >
          Lanjutkan
        </Button>
      </Grid>
    </Container>
  );
}

export default SplashScreen;
