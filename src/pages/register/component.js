import React, { useState, useEffect } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import AppBar from "@material-ui/core/AppBar";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { withRouter } from "react-router-dom";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import { register } from "../../services/register";
import { login } from "../../services/login";
import swal from "sweetalert";
import CircularProgress from "@material-ui/core/CircularProgress";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Dialog from "@material-ui/core/Dialog";
import useStyles from "./style";

function RegisterApp(props) {
  const [data, setData] = useState({
    name: "",
    email: "",
    password: "",
    password_confirmation: "",
    phone: "",
    nik: parseInt("")
  });
  const [errMail, setErrMail] = useState(false);
  const [errName, setErrName] = useState(false);
  const [errPassword, setErrorPassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [password, setPassword] = useState("");
  const [errPhone, setErrPhone] = useState(false);
  const [errNik, setErrNik] = useState(false);
  const [errConfirmation, setErrConfirmation] = useState(false);
  const [loading, setLoading] = useState(false);
  const classes = useStyles();

  const handleBack = () => {
    props.history.push("/login");
  };

  const handleChange = async e => {
    const newData = { ...data, [e.target.name]: e.target.value };
    setData(newData);
  };

  const handleChangePassword = e => {
    setPassword(e.target.value);
  };
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
    console.log(showPassword);
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const handleClick = () => {
    setLoading(true);

    register(data)
      .then(res => {
        console.log(res);
        const loginData = {
          email: data.email,
          password: data.password
        };
        console.log(loginData);
        login(loginData).then(res => {
          localStorage.setItem("user", JSON.stringify(res.user));
          localStorage.setItem("userToken", "Bearer " + res.access_token);
          localStorage.setItem("name", JSON.stringify(res.user.name));
          localStorage.setItem("nik", JSON.stringify(res.user.nik));
          localStorage.setItem("login", true);
          props.history.push("/");
          swal("Good job!", "You registered!", "success");
          console.log(res);
          props.history.push("/");
        });
      })
      .catch(error => {
        setLoading(false);
        if (error) {
          swal("Fill the form correctly,  please!");
        } else if (error.response.statusText == "Unauthenticated") {
          swal(
            "Ups!",
            "The token is expired or refresh the page, please",
            "warning"
          );
        } else if (error.response.statusText == "Unauthenticated") {
          swal(
            "Ups!",
            "The token is expired or refresh the page, please",
            "warning"
          );
        }
      });
  };
  const handleSignIn = () => {
    props.history.push("./login");
  };
  return (
    <Container maxWidth="xs" className={classes.Container}>
      <AppBar position="static" className={classes.appbar} onClick={handleBack}>
        <Toolbar>
          <ArrowBackIcon />
          <Typography className={classes.registrasi}> Registration </Typography>
        </Toolbar>
      </AppBar>

      <div align="center">
        <Grid item xs className={classes.nama}>
          <TextField
            error={errName}
            name="name"
            label="Nama Lengkap"
            value={data.name}
            onChange={handleChange}
            fullWidth={true}
            style={{ maxWidth: "90%" }}
            required
          />
        </Grid>

        <Grid item className={classes.textField}>
          <TextField
            error={errMail}
            fullWidth={true}
            onChange={handleChange}
            value={data.email}
            name="email"
            type="email"
            label="Email"
            fullWidth={true}
            style={{ maxWidth: "90%" }}
            required
          />
        </Grid>

        <Grid item className={classes.textField}>
          <TextField
            helperText="Sedikitnya 8 Karakter"
            error={errPassword}
            // type={showPassword ? "text" : "password"}
            type="password"
            name="password"
            label="Password"
            onChange={handleChange}
            value={data.password}
            fullWidth={true}
            style={{ maxWidth: "90%" }}
            required
          />
        </Grid>
        <Grid item className={classes.textField}>
          <TextField
            helperText="Sedikitnya 8 Karakter"
            error={errConfirmation}
            // type={showPassword ? "text" : "password"}
            type="password"
            name="password_confirmation"
            label="Password confirmation"
            onChange={handleChange}
            value={data.password_confirmation}
            fullWidth={true}
            style={{ maxWidth: "90%" }}
            required
          />
        </Grid>

        <Grid item className={classes.textField}>
          <TextField
            type="number"
            helperText="Setidaknya Memiliki 10 Karakter"
            error={errPhone}
            label="Nomor Telepon"
            name="phone"
            onChange={handleChange}
            value={data.phone}
            fullWidth={true}
            style={{ maxWidth: "90%" }}
            required
          />
        </Grid>

        <Grid item className={classes.textField}>
          <TextField
            type="number"
            label="NIK"
            name="nik"
            onChange={handleChange}
            value={data.nik}
            error={errNik}
            fullWidth={true}
            style={{ maxWidth: "90%" }}
            required
          />
        </Grid>

        <Grid item className={classes.gridBottom}>
          <Button
            style={{
              backgroundColor: "#F7A647"
            }}
            className={classes.registerButton}
            onClick={handleClick}
          >
            Register
          </Button>
        </Grid>
        <Grid item>
          <Typography className={classes.akun}>
            Sudah Punya Akun ?{" "}
            <b onClick={handleSignIn} style={{ color: "#F7A647" }}>
              Login
            </b>
          </Typography>
        </Grid>
      </div>
      <Dialog open={loading} onClose={() => setLoading(false)}>
        <DialogContent>
          <div align="center" style={{ margin: 10 }}>
            <CircularProgress />
          </div>

          <DialogContentText id="alert-dialog-description">
            Harap tunggu...
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </Container>
  );
}

export default withRouter(RegisterApp);
