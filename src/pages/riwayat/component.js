import React, { useEffect, useState } from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "../../component/appbar";
import ContentLoader from "react-content-loader";
import {
  getHistory,
  getListLoket,
  getQurrentListHistory
} from "../../services/loket";
import CircularProgress from "@material-ui/core/CircularProgress";
import useStyles from "./style";

const MyLoader = () => (
  <ContentLoader
    height={746}
    width={400}
    speed={2}
    primaryColor="#e6e6e6"
    secondaryColor="#f4f4f4"
  >
    <rect x="172" y="63" rx="4" ry="10" width="57" height="47" />
    <rect x="282" y="60" rx="4" ry="4" width="57" height="47" />
    <rect x="61" y="161" rx="4" ry="4" width="57" height="47" />
    <rect x="171" y="161" rx="4" ry="4" width="57" height="47" />
    <rect x="283" y="160" rx="4" ry="4" width="57" height="47" />
    <rect x="60" y="260" rx="4" ry="4" width="57" height="47" />
    <rect x="173" y="257" rx="4" ry="4" width="57" height="47" />
    <rect x="287" y="258" rx="4" ry="4" width="57" height="47" />
    <rect x="62" y="358" rx="4" ry="4" width="57" height="47" />
    <rect x="172" y="357" rx="4" ry="4" width="57" height="47" />
    <rect x="288" y="353" rx="4" ry="4" width="57" height="47" />
  </ContentLoader>
);

function Riwayat(props) {
  const [isLoading, setIsLoading] = useState(true);
  const [listHistory, setHistory] = useState([]);
  const [qurrentQueue, setQurrentQueue] = useState([]);
  const [qurrentList, setQurrentList] = useState([]);
  const user = JSON.parse(localStorage.getItem("user"));
  const classes = useStyles();

  useEffect(() => {
    const listHistory = async () => {
      const history = await getHistory(user.id);
      const loket = await getListLoket();
      const qurrent = await getQurrentListHistory(user.id);
      setQurrentList(qurrent);
      console.log("test", qurrent.row);
      setHistory(history.row.data);
      setQurrentQueue(loket.row);
    };

    listHistory().then(() => {
      setIsLoading(false);
    });
  }, []);
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="xs" className={classes.container}>
        <AppBar goBack title="Riwayat Kunjungan" />

        {isLoading == true ? (
          <div className={classes.loader}>
            <CircularProgress />
          </div>
        ) : (
          <Grid container spacing={0} style={{ paddingTop: "4em" }}>
            <Grid item xs={12} className={classes.gridTop}>
              <div style={{ paddingTop: "1em" }}>
                <Typography className={classes.textTop}>
                  Sedang Berjalan
                </Typography>
              </div>
              <div style={{ marginBottom: "2em" }}>
                {qurrentQueue.map(items => {
                  return (
                    <Grid container spacing={0} style={{ display: "flex" }}>
                      <Grid item xs={12} className={classes.itemBottom}>
                        <Grid className={classes.gridBox}>
                          <Grid container spacing={0}>
                            <Grid item xs={4} className={classes.itemContent}>
                              <Typography className={classes.content}>
                                Tanggal
                              </Typography>
                              <Typography className={classes.input}>
                                {items.today}
                              </Typography>
                            </Grid>
                            <Grid item xs={4} className={classes.itemContent}>
                              <Typography className={classes.content}>
                                No Antrian
                              </Typography>
                              <Typography className={classes.input}>
                                {items.current_queue ? items.current_queue : 0}{" "}
                                &nbsp;
                                {items.counter.id == 1 && "A"}
                                {items.counter.id == 2 && "B"}
                                {items.counter.id == 3 && "C"}
                                {items.counter.id == 5 && "D"}
                              </Typography>
                            </Grid>
                            <Grid item xs={4} className={classes.itemLoket}>
                              <Typography className={classes.content}>
                                Loket
                              </Typography>
                              <Typography className={classes.input}>
                                Loket {items.counter.id == 1 && "A"}
                                {items.counter.id == 2 && "B"}
                                {items.counter.id == 3 && "C"}
                                {items.counter.id == 5 && "D"}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  );
                })}
              </div>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={0}>
                <Grid item xs={7} className={classes.gridRiwayat}>
                  <Typography className={classes.textBottom}>
                    Riwayat Kunjungan
                  </Typography>
                </Grid>
                <Grid item xs={5} className={classes.gridRiwayat}>
                  <Typography className={classes.textNumber}>
                    ( {listHistory.length} )
                  </Typography>
                </Grid>
              </Grid>
              <div
                style={{
                  marginBottom: "5em"
                }}
              >
                {listHistory.map(items => {
                  return (
                    <Grid container spacing={0}>
                      <Grid item xs={12} className={classes.itemBottomHistory}>
                        <Grid className={classes.gridBoxBottom}>
                          <Grid container spacing={0}>
                            <Grid item xs={4} className={classes.itemContent}>
                              <Typography className={classes.content}>
                                Tanggal
                              </Typography>
                              <Typography className={classes.input}>
                                {items.queue_date}
                              </Typography>
                            </Grid>
                            <Grid item xs={4} className={classes.itemContent}>
                              <Typography className={classes.content}>
                                No Antrian
                              </Typography>
                              <Typography className={classes.input}>
                                {items.queue_number} &nbsp;
                                {items.counter_id == 1 && "A"}
                                {items.counter_id == 2 && "B"}
                                {items.counter_id == 3 && "C"}
                                {items.counter_id == 5 && "D"}
                              </Typography>
                            </Grid>
                            <Grid item xs={4} className={classes.itemLoket}>
                              <Typography className={classes.content}>
                                Loket
                              </Typography>
                              <Typography className={classes.input}>
                                Loket {items.counter_id == 1 && "A"}
                                {items.counter_id == 2 && "B"}
                                {items.counter_id == 3 && "C"}
                                {items.counter_id == 5 && "D"}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  );
                })}
              </div>
            </Grid>
          </Grid>
        )}
      </Container>
    </React.Fragment>
  );
}

export default Riwayat;
