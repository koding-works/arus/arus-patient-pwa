import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  container: {
    width: "100%",
    backgroundSize: "cover",
    padding: 0
  },
  gridUpper: {
    display: "flex",
    flexDirection: "column",
    marginTop: "17%",
    paddingBottom: "10%",
    backgroundColor: "#F1F1F1",
    paddingTop: "3%",
    width: "100%"
  },
  link: {
    textDecoration: "none"
  },
  itemList: {
    paddingTop: "4%"
  },
  loader: {
    marginTop: 70,
    width: "100%",
    backgroundColor: "white"
  },
  image: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "10%"
  },
  text: {
    color: "#9E9E9E",
    fontSize: "14px",
    marginTop: "2em"
  },
  loader: {
    marginTop: 200,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
}));
export default useStyles;
